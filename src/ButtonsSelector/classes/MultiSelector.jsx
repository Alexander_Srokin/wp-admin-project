import React from 'react';
import PropTypes from 'prop-types';
import ButtonItem from "./ButtonItem";

export default class MultiSelector extends React.Component {
    constructor(props) {
        super(props);

        this.validateData = this.validateData.bind(this);
        this.fetchButtonsList = this.fetchButtonsList.bind(this);
        this.updateComponent = this.updateComponent.bind(this);

        this.validateData();
    }

    static propTypes = {
        list: PropTypes.arrayOf(PropTypes.shape({
            caption: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired
        })).isRequired,
        value: PropTypes.array.isRequired,
        updateValue: PropTypes.func.isRequired,
        allowEmpty: PropTypes.bool.isRequired
    }

    validateData() {
        const {allowEmpty, list, value} = this.props;
        let activeKeysCount = 0;
        list.forEach((params, key) => {
            value.find(item => {if (item === params.value) activeKeysCount++});
        });
        if (!allowEmpty && !activeKeysCount) throw new Error("Минимум одна кнопка должна быть активна!");
    }

    /**
     * 1. Кнопка активна
     * 		- возможен ли пустой результат?
     * 			+) деактивируем кнопку
     * 			-) деактивируем кнопку если останется хоть одна активная кнопка
     * 	2. Кнопка неактивна
     * 		- активируем кнопку
     *
     * @param newActive boolean
     * @param  newValue array
     * @returns {boolean}
     */
    updateComponent(newActive, newValue) {
        const {value, allowEmpty, updateValue} = this.props;
        let result = value;
        if (newActive) result.push(newValue); else result = result.filter(item => item !== newValue);
        if (!allowEmpty&&!result.length) return false;
        updateValue(result);

        return true;
    }

    fetchButtonsList(list) {
        const {value} = this.props;
        let buttons = [];
        list.forEach((params, key) => {
            const filter = value.find(item => {return (item === params.value)});
            buttons.push(
                <ButtonItem
                    key={params.value}
                    {...params}
                    isActive={Boolean(filter)}
                    changeActive={this.updateComponent}
                />
            );
        });

        return buttons;
    }

    render() {
        const {list} = this.props;

        return <>
            {this.fetchButtonsList(list)}
        </>
    }
}
