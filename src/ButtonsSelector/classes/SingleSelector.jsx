import React from 'react';
import PropTypes from 'prop-types';
import ButtonItem from "./ButtonItem.jsx";

export default class SingleSelector extends React.Component {
    constructor(props) {
        super(props);

        this.validateData = this.validateData.bind(this);
        this.fetchButtonsList = this.fetchButtonsList.bind(this);
        this.updateComponent = this.updateComponent.bind(this);

        this.validateData();
    }

    static propTypes = {
        list: PropTypes.arrayOf(PropTypes.shape({
            caption: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired
        })).isRequired,
        value: PropTypes.string.isRequired,
        updateValue: PropTypes.func.isRequired,
        allowEmpty: PropTypes.bool.isRequired
    }

    validateData() {
        const {value, allowEmpty, list} = this.props;
        let activeKey = null;
        list.forEach((params, key) => {
            if (params.value === value) {
                if (activeKey != null) throw new Error("Нельзя иметь более одной активной кнопки!");
                activeKey = key;
            }
        });
        if (!allowEmpty && (activeKey == null)) throw new Error("Минимум одна кнопка должна быть активна!");
    }

    /**
     * @param newActive boolean
     * @param newValue string
     * @returns {boolean}
     */
    updateComponent(newActive, newValue) {
        const {allowEmpty, updateValue} = this.props;
        if (!newActive) {
            if (!allowEmpty) return false;
            updateValue("");
            return true;
        }
        updateValue(newValue);

        return true;
    }

    fetchButtonsList(list) {
        const {value} = this.props;
        let buttons = [];
        list.forEach((params) => {
            buttons.push(
                <ButtonItem
                    key={params.value}
                    {...params}
                    isActive={params.value === value}
                    changeActive={this.updateComponent}
                />
            );
        });

        return buttons;
    }

    render() {
        const {list} = this.props;
        return <>
            {this.fetchButtonsList(list)}
        </>
    }
}
