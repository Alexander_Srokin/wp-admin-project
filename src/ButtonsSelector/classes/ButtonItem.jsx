import React from 'react';
import PropTypes from 'prop-types';
import {Button} from '@wordpress/components';

export default class ButtonItem extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    static propTypes = {
        caption: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
        isActive: PropTypes.bool.isRequired,
        changeActive: PropTypes.func.isRequired,
    }

    handleClick(e) {
        const {changeActive, value, isActive} = this.props;
        changeActive(!isActive, value);
    }

    render() {
        const {caption, isActive} = this.props;

        return <Button
            isLarge
            isPrimary={isActive}
            onClick={this.handleClick}
        >
            {caption}
        </Button>;
    }
}


