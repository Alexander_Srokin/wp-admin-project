import React from 'react';
import PropTypes from 'prop-types';
import {ButtonGroup} from '@wordpress/components';
import SingleSelector from './classes/SingleSelector.jsx';
import MultiSelector from './classes/MultiSelector.jsx';

export default class ButtonsSelector extends React.Component {
    constructor(props) {
        super(props);

        this.getValue = this.getValue.bind(this);
        this.updateList = this.updateList.bind(this);
    }

    static propTypes = {
        propertyName: PropTypes.string.isRequired,
        list: PropTypes.arrayOf(PropTypes.shape({
            caption: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired
        })).isRequired,
        value: PropTypes.any,
        handleUpdate: PropTypes.func.isRequired,
        multi: PropTypes.bool,
        allowEmpty: PropTypes.bool
    }

    static defaultProps = {
        value: null,
        multi: false,
        allowEmpty: false,
    }

    getValue() {
        const {value, multi} = this.props;
        if (value === false) return (multi) ? [] : '';

        return value;
    }

    updateList(value) {
        const {propertyName, handleUpdate} = this.props;
        let responseObject = {}
        responseObject[propertyName] = value;
        handleUpdate(responseObject);
    }

    render() {
        const {list, multi, allowEmpty} = this.props;

        return <ButtonGroup>
            {multi ?
                <MultiSelector
                    list={list}
                    value={this.getValue()}
                    allowEmpty={allowEmpty}
                    updateValue={this.updateList}
                />
                :
                <SingleSelector
                    list={list}
                    value={this.getValue()}
                    allowEmpty={allowEmpty}
                    updateValue={this.updateList}
                />
            }
        </ButtonGroup>;
    }
}