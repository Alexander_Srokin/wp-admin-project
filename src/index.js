//import {hot} from 'react-hot-loader/root'
import ReactDOM from 'react-dom'
import React from 'react'
import ButtonsSelector from "./ButtonsSelector"

const COLOR_SETTINGS = [
    {
        caption: "Желный",
        value: "yellow"
    },
    {
        caption: "Синий",
        value: "blue"
    },
    {
        caption: "Зеленый",
        value: "green"
    },
    {
        caption: "Красный",
        value: "red"
    },
];

class TestComponent extends React.Component {
    constructor(props) {
        super(props);

        this.renderProperties = this.renderProperties.bind(this);

        this.state = {
            style: "green",
            styleE: "green",
            styleM: ["green"],
            styleME: ["green"],
        }
    }

    renderProperties() {
        let list = [];
        let key = 0;
        for (let property in this.state) {
            if (this.state.hasOwnProperty(property)) {
                const propertyValue = (Array.isArray(this.state[property])) ? this.state[property].join(', ') : this.state[property];
                list.push(<li key={key++}><b>{property}:</b> {propertyValue}</li>);
            }
        }

        return list;
    }

    render() {
        const {style, styleE, styleM, styleME} = this.state;

        return <>
            <h2>Свойства компонента:</h2>
            <ul>
                {this.renderProperties()}
            </ul>
            <br/><br/>
            <ButtonsSelector
                propertyName="style"
                list={COLOR_SETTINGS}
                value={style}
                handleUpdate={newState => this.setState(newState)}
            />
            <br/>
            <ButtonsSelector
                propertyName="styleE"
                list={COLOR_SETTINGS}
                value={styleE}
                handleUpdate={newState => this.setState(newState)}
                allowEmpty={true}
            />
            <br/>
            <ButtonsSelector
                propertyName="styleM"
                list={COLOR_SETTINGS}
                value={styleM}
                handleUpdate={newState => this.setState(newState)}
                multi={true}
            />
            <br/>
            <ButtonsSelector
                propertyName="styleME"
                list={COLOR_SETTINGS}
                value={styleME}
                handleUpdate={newState => this.setState(newState)}
                multi={true}
                allowEmpty={true}
            />
        </>
    }
}

ReactDOM.render(
    <>
        <TestComponent/>
    </>,
    document.getElementById('root')
);