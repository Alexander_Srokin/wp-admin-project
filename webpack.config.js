var
    path = require('path'),
    webpack = require('webpack'),
    MiniCssExtractPlugin = require('mini-css-extract-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')
    // autoprefixer = require('autoprefixer'),
;

module.exports = function (env) {
    console.log(env);
    var
        NODE_ENV = env.NODE_ENV || 'dev',
        NODE_ENV_IS_DEV = NODE_ENV === 'dev'
    ;

    var config = {
        stats: {
            children: false
        },
        mode: NODE_ENV_IS_DEV ? 'development' : 'production',
        watch: NODE_ENV_IS_DEV,
        watchOptions: {
            ignored: /node_modules/,
        },
        //context: path.resolve(__dirname, './src'),
        entry: {
            main: './src/index.js',
            //render: './render.js'
        },
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: NODE_ENV_IS_DEV ? '[name].js' : '[name].min.js'
        },
        externals: {
            //'react': 'React',
            //'react-dom': 'ReactDOM',
        },
        module: {
            rules: [
                {
                    test: /\.css?$/,
                    use: [
                        (NODE_ENV_IS_DEV ? 'style-loader' : MiniCssExtractPlugin.loader),
                        'css-loader'
                    ]
                },
                {
                    test: /\.less?$/,
                    use: [
                        (NODE_ENV_IS_DEV ? 'style-loader' : MiniCssExtractPlugin.loader),
                        'css-loader',
                        "postcss-loader",
                        {
                            loader: 'less-loader',
                            options: {
                                javascriptEnabled: true
                            }
                        }
                    ]
                },
                /*{
                    test: /\.(html)$/,
                    loader: 'html-loader',
                },*/
                /*{
                    test: /\.(html)$/,
                    loader: 'html-loader',
                    options: {
                        // Disables attributes processing
                        attributes: false,
                    },
                },*/
                {
                    test: /\.(html)$/,
                    use: {
                        loader: 'html-loader',
                        options: {
                            interpolate: true,
                        },
                    },
                },
                {
                    test: /\.(js|jsx)$/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                cacheDirectory: true,
                                /*presets: [
                                    [
                                        '@babel/preset-env',
                                        {
                                            targets: {
                                                browsers: 'last 2 versions',
                                            },
                                        },
                                    ],
                                    '@babel/preset-typescript',
                                    '@babel/preset-react',
                                ],
                                plugins: [
                                    [
                                        '@babel/plugin-proposal-decorators',
                                        {legacy: true},
                                    ],
                                    [
                                        '@babel/plugin-proposal-class-properties',
                                        {loose: true},
                                    ],
                                    ['@babel/plugin-transform-runtime'],
                                    'react-hot-loader/babel',
                                ],*/
                            },
                        },
                    ],
                },
                {
                    test: /\.(png|jpe?g|jpg|woff|woff2)$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[path][name].[ext]'
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            //new webpack.ProvidePlugin({
                //React: 'react',
            //}),
            new HtmlWebpackPlugin({
                template: './index.html',
                // chunks: ['index']
            }),
            new ScriptExtHtmlWebpackPlugin({
                defaultAttribute: 'defer',
            }),
        ],
        devtool: 'source-map',
        devServer: {
            contentBase: './',
            //port:9000,
            hot: true,
            historyApiFallback: true,
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.jsx', '.html']
        },
    };

    if (!NODE_ENV_IS_DEV)
        config.plugins.push(new MiniCssExtractPlugin({}));

    return config
};